1.1
$x("//Season")

1.2
$x("//SkierLogs/Season/Skiers/Skier[@userName='mari_dahl']/Log/Entry")

1.3
$x("//SkierLogs/Season[@fallYear='2015']/Skiers[@clubId='vindil']/Skier/Log[Entry/Distance/text()>10]")

1.4
$x("//SkierLogs/Season[@fallYear='2016']/Skiers/Skier/@userName[contains(../Log, 'Venabygd')]")

1.5
$x("count(//SkierLogs/Skiers/Skier[YearOfBirth<2005 and YearOfBirth > 2001])")

1.6
$x("//SkierLogs/Season[@fallYear='2016']/Skiers/Skier[@userName='idar_kals1']/Log/Entry/Date[contains(../Area, 'Lygna')]")

1.7
$x("sum(//SkierLogs/Season[@fallYear='2015']/Skiers/Skier/Log/Entry/Distance)")

1.8
$x("count(//SkierLogs/Season[@fallYear='2016']/Skiers[not(@clubId)]/Skier/Log/Entry/Distance)")

1.9
$x("//SkierLogs/Skiers/Skier[@userName = ../../Season[@fallYear='2015']/Skiers[@clubId=../../Clubs/Club/@id[../County='Oppland']]/Skier/@userName]")

1.10
$x("//SkierLogs/Skiers/Skier[@userName = ../../Season[@fallYear='2015']/Skiers/Skier/@userName[../Log/Entry/Area ='Nordseter'] and not (@userName = ../../Season[@fallYear='2016']/Skiers/Skier/@userName[../Log/Entry/Area ='Nordseter'])]")
